package pl.mradtke.secondpresentation.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import pl.mradtke.secondpresentation.AppController;
import pl.mradtke.secondpresentation.utils.NetworkUtil;
import pl.mradtke.secondpresentation.utils.Utils;

/**
 * Created by Michal Radtke on 2015-05-08.
 * <p/>
 * This receiver monitors internet connection changes.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Before displays network status ensure that application is in foreground
        if (AppController.isActivityVisible()) {
            String status = NetworkUtil.getConnectivityStatus(context);
            Utils.showToast(status);
        }
    }
}
