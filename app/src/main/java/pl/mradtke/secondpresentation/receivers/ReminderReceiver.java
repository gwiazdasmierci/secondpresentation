package pl.mradtke.secondpresentation.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.fragments.TodoElementFragment;

/**
 * Created by Michal Radtke on 2015-05-22.
 * <p/>
 * This broadcast is called when reminder time is elapsed.
 */
public class ReminderReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = ReminderReceiver.class.getSimpleName();
    private static final int NOTIFICATION_ID = 202;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Bundle bundle = intent.getExtras();
            String reminderTitle = bundle.getString(TodoElementFragment.EXTRA_REMINDER_TITLE);
            String reminderText = bundle.getString(TodoElementFragment.EXTRA_REMINDER_TEXT);
            createNotification(context, reminderTitle, reminderText);
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    /**
     * This method prepares notification with information about reminder: title and text. It is called when reminder
     * time is elapsed.
     *
     * @param context       is need to get system services
     * @param reminderTitle is a reminder title
     * @param reminderText  is a reminder text
     */
    private void createNotification(Context context, String reminderTitle, String reminderText) {
        Notification.Builder builder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.mail_notification)
                .setContentTitle(reminderTitle)
                .setContentText(reminderText);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context
                .NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
}
