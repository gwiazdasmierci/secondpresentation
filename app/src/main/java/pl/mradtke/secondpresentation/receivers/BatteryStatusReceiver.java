package pl.mradtke.secondpresentation.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.services.BatteryStatusService;
import pl.mradtke.secondpresentation.utils.Utils;

/**
 * Created by Michal Radtke on 2015-05-27.
 * <p/>
 * This receiver gets current battery status from registered service and shows it in simple toast.
 */
public class BatteryStatusReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() != null) {
            String batteryStatus = String.format(context.getString(R.string.receiver_battery_status),
                    intent.getExtras().getInt(BatteryStatusService.EXTRA_BATTERY_STATUS));
            Utils.showToast(batteryStatus);
        }
    }
}
