package pl.mradtke.secondpresentation.views;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import pl.mradtke.secondpresentation.AppController;
import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.SessionManager;
import pl.mradtke.secondpresentation.fragments.FileExplorerFragment;
import pl.mradtke.secondpresentation.fragments.GalleryFragment;
import pl.mradtke.secondpresentation.fragments.LogInFragment;
import pl.mradtke.secondpresentation.fragments.MainFragment;
import pl.mradtke.secondpresentation.fragments.NotepadFragment;
import pl.mradtke.secondpresentation.fragments.PhotoFragment;
import pl.mradtke.secondpresentation.fragments.TimePickerDialogFragment;
import pl.mradtke.secondpresentation.fragments.TodoElementFragment;
import pl.mradtke.secondpresentation.fragments.TodoListFragment;
import pl.mradtke.secondpresentation.receivers.BatteryStatusReceiver;
import pl.mradtke.secondpresentation.services.BatteryStatusService;
import pl.mradtke.secondpresentation.utils.Utils;

/**
 * Created by Michal Radtke on 2015-05-05.
 * <p/>
 * Main Activity.
 */
public class MainActivity extends Activity {

    private static final int INDEX_OF_LOG_IN_FRAGMENT_IN_BACK_STACK = 1;
    private BatteryStatusReceiver batteryStatusReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }

            SessionManager sessionManager = new SessionManager(AppController.getInstance().getApplicationContext());
            if (sessionManager.isLoggedIn()) {
                showMainFragment();
            } else {
                showLogInFragment();
            }
            batteryStatusReceiver = new BatteryStatusReceiver();
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > INDEX_OF_LOG_IN_FRAGMENT_IN_BACK_STACK) {
            fragmentManager.popBackStack();
        } else {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.activityPaused();

        //unregister battery status service (stop it)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(batteryStatusReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.activityResumed();

        //setup, register and start battery status service
        IntentFilter batteryStatusIntentFilter = new IntentFilter(BatteryStatusService.BROADCAST_BATTERY_STATUS_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(batteryStatusReceiver, batteryStatusIntentFilter);
        Intent batteryStatusServiceIntent = new Intent(this, BatteryStatusService.class);
        startService(batteryStatusServiceIntent);
    }

    /**
     * This method replaces actually visible fragment with LogInFragment.
     */
    public void showLogInFragment() {
        Fragment logInFragment = new LogInFragment();
        Utils.replaceFragmentWithoutBackStack(getFragmentManager().beginTransaction(), logInFragment,
                LogInFragment.class.getSimpleName());
    }

    /**
     * This method replaces actually visible fragment with MainFragment.
     */
    public void showMainFragment() {
        Fragment mainFragment = new MainFragment();
        Utils.replaceFragment(getFragmentManager().beginTransaction(), mainFragment,
                MainFragment.class.getSimpleName());
    }

    /**
     * This method replaces actually visible fragment with NotepadFragment.
     *
     * @param filename is a name of file which user want to read/edit
     */
    public void showNotepadFragment(String filename) {
        NotepadFragment notepadFragment = NotepadFragment.newInstance(filename);
        Utils.replaceFragment(getFragmentManager().beginTransaction(), notepadFragment,
                NotepadFragment.class.getSimpleName());
    }

    /**
     * This method replaces actually visible fragment with TodoListFragment.
     */
    public void showTodoListFragment() {
        TodoListFragment todoListFragment = new TodoListFragment();
        Utils.replaceFragment(getFragmentManager().beginTransaction(), todoListFragment,
                TodoListFragment.class.getSimpleName());
    }

    /**
     * This method replaces actually visible fragment with GalleryFragment.
     */
    public void showGalleryFragment() {
        GalleryFragment galleryFragment = new GalleryFragment();
        Utils.replaceFragment(getFragmentManager().beginTransaction(), galleryFragment,
                GalleryFragment.class.getSimpleName());
    }

    /**
     * This method replaces actually visible fragment with FileExplorerFragment.
     */
    public void showFileExplorerFragment() {
        FileExplorerFragment fileExplorerFragment = new FileExplorerFragment();
        Utils.replaceFragment(getFragmentManager().beginTransaction(), fileExplorerFragment,
                FileExplorerFragment.class.getSimpleName());
    }

    /**
     * This method replaces actually visible fragment with TodoElementFragment.
     *
     * @param todoTitle is a title of to do which user want to edit
     */
    public void showTodoElementFragment(String todoTitle) {
        TodoElementFragment todoElementFragment = TodoElementFragment.newInstance(todoTitle);
        Utils.replaceFragment(getFragmentManager().beginTransaction(), todoElementFragment,
                TodoElementFragment.class.getSimpleName());
    }

    /**
     * This method shows time picker dialog on visible fragment.
     */
    public void showTimePickerDialog() {
        DialogFragment timePickerDialog = new TimePickerDialogFragment();
        timePickerDialog.show(getFragmentManager(), TimePickerDialogFragment.class.getSimpleName());
    }

    /**
     * This method replaces actually visible fragment with PhotoFragment.
     *
     * @param photoUrl is an url of photo which user want to display in a full size
     */
    public void showPhotoFragment(String photoUrl) {
        PhotoFragment photoFragment = PhotoFragment.newInstance(photoUrl);
        Utils.replaceFragment(getFragmentManager().beginTransaction(), photoFragment, PhotoFragment.class.getSimpleName());
    }
}
