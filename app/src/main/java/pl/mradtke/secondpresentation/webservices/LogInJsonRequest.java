package pl.mradtke.secondpresentation.webservices;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pl.mradtke.secondpresentation.AppController;
import pl.mradtke.secondpresentation.SessionManager;
import pl.mradtke.secondpresentation.fragments.MainFragment;
import pl.mradtke.secondpresentation.utils.NetworkUtil;
import pl.mradtke.secondpresentation.utils.Utils;

/**
 * Created by Michal Radtke on 2015-05-05.
 * <p/>
 * This class is responsible for prepare and send login request to server.
 */
public class LogInJsonRequest extends AsyncTask<String, Void, Void> {

    private static final String LOG_TAG = LogInJsonRequest.class.getSimpleName();
    private static final String JSON_KEY_ERROR = "error";
    private static final String JSON_KEY_ERROR_MESSAGE = "error_msg";
    private static final String JSON_KEY_USER_ID = "id";
    private static final String JSON_KEY_LOGIN = "login";
    private static final String JSON_KEY_PASSWORD = "password";
    private static final String JSON_TAG_KEY = "tag";
    private static final String JSON_TAG_VALUE = "login";
    private static final String REQUEST_TAG = "request_login";

    private Activity activity;
    private SessionManager sessionManager;

    public LogInJsonRequest(Activity activity) {
        this.activity = activity;
        sessionManager = new SessionManager(AppController.getInstance().getApplicationContext());
    }

    @Override
    protected Void doInBackground(String... params) {
        checkLogin(params[0], params[1]);
        return null;
    }

    /**
     * This method prepares and sends register request to server. Also waits for response if the
     * process is finished without problems.
     *
     * @param login    is a login parameter of request.
     * @param password is a password parameter of request.
     */
    private void checkLogin(final String login, final String password) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkUtil.URL_LOGIN,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d(LOG_TAG, "Login response: " + response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean error = jsonObject.getBoolean(JSON_KEY_ERROR);

                            //check for error node in json
                            if (!error) {
                                //user successfully logged in
                                //create login session
                                String userId = jsonObject.getString(JSON_KEY_USER_ID);
                                sessionManager.setLogin(true, userId);

                                //goto main fragment
                                MainFragment mainFragment = new MainFragment();
                                Utils.replaceFragment(activity.getFragmentManager().beginTransaction(),
                                        mainFragment, MainFragment.class.getSimpleName());
                            } else {
                                //error in login, get the error message
                                Utils.showToast(jsonObject.getString(JSON_KEY_ERROR_MESSAGE));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(LOG_TAG, "Login error: " + volleyError.getMessage());
                Utils.showToast(volleyError.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> requestParams = new HashMap<>();
                requestParams.put(JSON_TAG_KEY, JSON_TAG_VALUE);
                requestParams.put(JSON_KEY_LOGIN, login);
                requestParams.put(JSON_KEY_PASSWORD, password);

                return requestParams;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, REQUEST_TAG);
    }
}
