package pl.mradtke.secondpresentation.webservices;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pl.mradtke.secondpresentation.AppController;
import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.utils.NetworkUtil;
import pl.mradtke.secondpresentation.utils.Utils;

/**
 * Created by Michal Radtke on 2015-05-05.
 * <p/>
 * This class is responsible for prepare and send register request to server.
 */
public class RegisterJsonRequest extends AsyncTask<String, Void, Void> {

    private final static String LOG_TAG = RegisterJsonRequest.class.getSimpleName();
    private static final String JSON_KEY_ERROR = "error";
    private static final String JSON_KEY_ERROR_MESSAGE = "error_msg";
    private static final String JSON_KEY_EMAIL = "email";
    private static final String JSON_KEY_LOGIN = "login";
    private static final String JSON_KEY_PASSWORD = "password";
    private static final String JSON_TAG_KEY = "tag";
    private static final String JSON_TAG_VALUE = "register";
    private static final String REQUEST_TAG = "request_register";

    private Activity activity;
    private ProgressDialog progressDialog;

    public RegisterJsonRequest(Activity activity) {
        this.activity = activity;
        progressDialog = new ProgressDialog(activity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setCancelable(false);
        progressDialog.setMessage(activity.getString(R.string.register_json_request_registering_process_message));
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(String... params) {
        registerUser(params[0], params[1], params[2]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();
    }

    /**
     * This method prepares and sends register request to server. Also waits for response if the
     * process is finished without problems.
     *
     * @param login    is a login parameter of request.
     * @param password is a password parameter of request.
     * @param email    is an email parameter of request.
     */
    private void registerUser(final String login, final String password, final String email) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                NetworkUtil.URL_REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(LOG_TAG, "Register response: " + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean(JSON_KEY_ERROR);
                    if (!error) {
                        //user successfully registered action
                        Utils.showToast(activity.getString(R.string
                                .register_json_request_registration_confirmation_message));
                        activity.getFragmentManager().popBackStack();
                    } else {
                        //error occurred in registration, get the error message
                        Utils.showToast(jsonObject.getString(JSON_KEY_ERROR_MESSAGE));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(LOG_TAG, "Registration error: " + volleyError.getMessage());
                Utils.showToast(volleyError.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> requestParams = new HashMap<>();
                requestParams.put(JSON_TAG_KEY, JSON_TAG_VALUE);
                requestParams.put(JSON_KEY_LOGIN, login);
                requestParams.put(JSON_KEY_PASSWORD, password);
                requestParams.put(JSON_KEY_EMAIL, email);

                return requestParams;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, REQUEST_TAG);
    }
}
