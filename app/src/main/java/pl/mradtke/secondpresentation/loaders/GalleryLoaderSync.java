package pl.mradtke.secondpresentation.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import pl.mradtke.secondpresentation.models.GalleryDataModel;
import pl.mradtke.secondpresentation.utils.NetworkUtil;
import pl.mradtke.secondpresentation.webservices.ServiceHandler;

/**
 * Created by michal.radtke@gmail.com on 2015-05-22.
 * <p/>
 * This class downloads data from JSON.
 */
public class GalleryLoaderSync extends AsyncTaskLoader<ArrayList<GalleryDataModel>> {

    private static final String LOG_TAG = GalleryLoaderSync.class.getSimpleName();
    private static final String JSON_TAG_KEY = "gallery";
    private static final String JSON_KEY_TITLE = "title";
    private static final String JSON_KEY_DESC = "description";
    private static final String JSON_KEY_IMAGE = "image_url";
    private static final String JSON_KEY_IMAGE_THUMB = "image_thumb_url";

    private ArrayList<GalleryDataModel> galleryList;

    public GalleryLoaderSync(Context context) {
        super(context);
        galleryList = new ArrayList<>();
    }

    @Override
    public ArrayList<GalleryDataModel> loadInBackground() {
        ServiceHandler serviceHandler = new ServiceHandler();
        String jsonStr = serviceHandler.makeServiceCall(NetworkUtil.URL_GALLERY, ServiceHandler.POST);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONArray jsonArray = jsonObj.getJSONArray(JSON_TAG_KEY);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    galleryList.add(i, prepareGalleryListElement(jsonObject.getString(JSON_KEY_TITLE),
                            jsonObject.getString(JSON_KEY_DESC),
                            jsonObject.getString(JSON_KEY_IMAGE),
                            NetworkUtil.GLOBAL_URL + jsonObject.getString(JSON_KEY_IMAGE_THUMB)));
                }
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.e(LOG_TAG, "Couldn't get any data from the url");
        }
        return galleryList;
    }

    @Override
    public void deliverResult(ArrayList<GalleryDataModel> galleryListData) {
        super.deliverResult(galleryListData);
    }

    /**
     * This method prepares a gallery data model which will be an element of gallery list.
     *
     * @param title    is an image title
     * @param desc     is an image description
     * @param imageUrl is an image url
     * @param thumbUrl is an image thumb url
     * @return prepared gallery data element list
     * @throws IOException related with url parsing and downloading bitmap
     */
    private GalleryDataModel prepareGalleryListElement(String title, String desc, String imageUrl,
                                                       String thumbUrl) throws IOException {
        GalleryDataModel galleryDataModel = new GalleryDataModel();
        galleryDataModel.setImageTitle(title);
        galleryDataModel.setImageDesc(desc);
        galleryDataModel.setImageUrl(imageUrl);
        URL url = new URL(thumbUrl);
        Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        galleryDataModel.setImageThumb(bitmap);

        return galleryDataModel;
    }
}

