package pl.mradtke.secondpresentation.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Michal Radtke on 2015-05-27.
 * <p/>
 * This loader is responsible for caching full size image from passed url.
 */
public class ImageLoaderSync extends AsyncTaskLoader<Bitmap> {

    private Bitmap photo;
    private String photoUrl;

    public ImageLoaderSync(Context context, String photoUrl) {
        super(context);
        this.photoUrl = photoUrl;
    }

    /**
     * This method downloads image from url and stores it in cache.
     *
     * @return prepared bitmap
     */
    @Override
    public Bitmap loadInBackground() {
        URL url;
        try {
            url = new URL(photoUrl);
            photo = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return photo;
    }

    @Override
    public void deliverResult(Bitmap photo) {
        super.deliverResult(photo);
    }
}
