package pl.mradtke.secondpresentation.utils;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.widget.Toast;

import pl.mradtke.secondpresentation.AppController;
import pl.mradtke.secondpresentation.R;

/**
 * Created by Michal Radtke on 2015-05-06.
 * <p/>
 * It is a method with global app utils.
 */
public class Utils {

    /**
     * This method is responsible for replacing fragment in container with registering in back
     * stack.
     *
     * @param fragmentTransaction is coming from active fragment manager from activity and it begins
     *                            fragment transaction.
     * @param fragment            is a fragment which will be replace.
     * @param fragmentTag         is a fragment tag which allows to recognize fragment in fragment
     *                            manager.
     */
    public static void replaceFragment(FragmentTransaction fragmentTransaction, Fragment fragment,
                                       String fragmentTag) {
        fragmentTransaction.replace(R.id.fragment_container, fragment, fragmentTag);
        fragmentTransaction.addToBackStack(fragmentTag);
        fragmentTransaction.commit();
    }

    /**
     * This method is responsible for replacing fragment in container without registering in back stack.
     *
     * @param fragmentTransaction is coming from active fragment manager from activity and it begins fragment
     *                            transaction.
     * @param fragment            is a fragment which will be replace.
     * @param fragmentTag         is a fragment tag which allows to recognize fragment in fragment manager.
     */
    public static void replaceFragmentWithoutBackStack(FragmentTransaction fragmentTransaction, Fragment fragment,
                                                       String fragmentTag) {
        fragmentTransaction.replace(R.id.fragment_container, fragment, fragmentTag);
        fragmentTransaction.commit();
    }

    /**
     * This method displays toast on a screen.
     *
     * @param text is a text which will be display.
     */
    public static void showToast(String text) {
        Toast.makeText(AppController.getInstance().getApplicationContext(), text, Toast.LENGTH_LONG)
                .show();
    }
}
