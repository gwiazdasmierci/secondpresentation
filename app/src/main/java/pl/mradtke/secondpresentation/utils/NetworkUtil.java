package pl.mradtke.secondpresentation.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import pl.mradtke.secondpresentation.R;

/**
 * Created by Michal Radtke on 2015-05-08.
 * <p/>
 * This class is responsible for getting internet connection status and type.
 */
public class NetworkUtil {

    public static final String GLOBAL_URL = "http://192.168.34.216";
    public static final String URL_LOGIN = GLOBAL_URL + "/mobile_api/";
    public static final String URL_REGISTER = GLOBAL_URL + "/mobile_api/";
    public static final String URL_GALLERY = GLOBAL_URL + "/mobile_api/json.php?gallery=all";

    /**
     * Gets a type of connection status.
     *
     * @param context is needs to get system services.
     * @return type of connection status.
     */
    public static String getConnectivityStatus(Context context) {
        String status = context.getString(R.string.network_util_not_connected_message);
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                status = context.getString(R.string.network_util_wifi_enabled_message);
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                status = context.getString(R.string.network_util_mobile_data_enabled_message);
            }
        }
        return status;
    }

    /**
     * This method checks if device has an internet connection.
     *
     * @param context is needs to get system services.
     * @return internet connection status.
     */
    public static boolean isInternetConnection(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
