package pl.mradtke.secondpresentation.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pl.mradtke.secondpresentation.R;

/**
 * Created by Michal Radtke on 2015-05-08.
 * <p/>
 * This util class is related with file actions.
 */
public class FileUtil {

    private static final String LOG_TAG = FileUtil.class.getSimpleName();
    private static final String APP_EXTERNAL_CATALOGUE = "/SecondPresentation";
    private static final String FILE_EXTENSION = ".txt";
    public static final String FILE_PATH = Environment.getExternalStorageDirectory() + APP_EXTERNAL_CATALOGUE + "/";

    /**
     * This method is responsible for file updating action.
     *
     * @param context     application context
     * @param filename    name of file
     * @param fileContent content of file
     */
    public static void updateFile(Context context, String filename, String fileContent) {
        File file = new File(FILE_PATH, filename);
        saveFile(file, fileContent);
        Utils.showToast(context.getString(R.string.file_util_file_update_successfully));
    }

    /**
     * This method is responsible for file saving action.
     *
     * @param context     application context
     * @param filename    name of file
     * @param fileContent content of file
     */
    public static void saveAsFile(Context context, String filename, String fileContent) {
        File file = new File(FILE_PATH, filename + FILE_EXTENSION);
        saveFile(file, fileContent);
        Utils.showToast(context.getString(R.string.file_util_file_saved_successfully));
    }

    /**
     * This method is responsible for getting file content from url.
     *
     * @param filename is a name of file to save
     * @param url      is an url from will be reading file content
     */
    public static void saveAsFileFromUrl(final String filename, final String url) {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        executorService.execute(new Runnable() {
            public void run() {
                try {
                    URL fileUrl = new URL(url);
                    BufferedReader fileContent = new BufferedReader(new InputStreamReader(fileUrl.openStream()));
                    StringBuilder text = new StringBuilder();

                    String inputLine;
                    while ((inputLine = fileContent.readLine()) != null) {
                        text.append(inputLine);
                    }
                    fileContent.close();
                    File file = new File(FILE_PATH, filename);
                    saveFile(file, text.toString());
                } catch (IOException e) {
                    Log.i(LOG_TAG, "Error during reading content from url");
                    e.printStackTrace();
                }
            }
        });
        executorService.shutdown();
    }

    /**
     * This class saves a file.
     *
     * @param file        file instance
     * @param fileContent file content
     */
    private static void saveFile(File file, String fileContent) {
        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(fileContent.getBytes());
            outputStream.close();
        } catch (java.io.IOException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    /**
     * This method is responsible for loading file content.
     *
     * @param filename name of file
     * @return content of loaded file
     */
    public static String loadFromFile(String filename) {
        File file = new File(FILE_PATH, filename);
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                text.append(line);
            }
            bufferedReader.close();
        } catch (IOException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
        return text.toString();
    }

    /**
     * Create application catalogue on external storage if not exists.
     */
    public static void createExternalPathIfNotExists() {
        File externalPath = new File(Environment.getExternalStorageDirectory() + APP_EXTERNAL_CATALOGUE);
        if (!externalPath.exists()) {
            externalPath.mkdirs();
        }
    }

    /**
     * This method gets list of files in specified directory with defined options.
     *
     * @param filesList  is a files list available in specified directory
     * @param onlyDirs   is an option which gets only catalogues list (it misses files)
     * @param showHidden is an option which gets hidden files and catalogues too.
     * @return sorted list of files
     */
    private static ArrayList<File> filesFilter(File[] filesList, boolean onlyDirs, boolean showHidden) {
        ArrayList<File> files = new ArrayList<>();
        for (File file : filesList) {
            if (onlyDirs && !file.isDirectory())
                continue;
            if (!showHidden && file.isHidden())
                continue;
            files.add(file);
        }
        Collections.sort(files);
        return files;
    }

    /**
     * This method returns only file names from given files list.
     *
     * @param filesList is a files list from which method gets file names
     * @return list of file names
     */
    public static ArrayList<String> fileNames(File[] filesList) {
        ArrayList<File> files = filesFilter(filesList, false, false);
        ArrayList<String> names = new ArrayList<>();
        for (int i = 0; i < files.size(); i++) {
            names.add(i, files.get(i).getName());
        }
        return names;
    }

    /**
     * This method deletes file.
     *
     * @param filename is a filename to delete
     */
    public static void deleteFile(String filename) {
        File file = new File(FILE_PATH + filename);
        file.delete();
    }
}
