package pl.mradtke.secondpresentation;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Michal Radtke on 2015-05-06.
 * <p/>
 * This method is a global application method for this application.
 */
public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();

    private static AppController instance;
    private RequestQueue requestQueue;
    private static boolean activityVisible;

    public static synchronized AppController getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    /**
     * This method gets request from the request queue after the whole processes of requests
     * preparing.
     *
     * @return request queue.
     */
    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return requestQueue;
    }

    /**
     * This method adds request with its tag to the request queue after request preparing process.
     *
     * @param request is a request to add to the request queue.
     * @param tag     is a tag which marks actual request.
     * @param <T>     is a request type.
     */
    public <T> void addToRequestQueue(Request<T> request, String tag) {
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(request);
    }

    /**
     * This method adds request to the request queue after request preparing process.
     *
     * @param request is a request to add to the request queue.
     * @param <T>     is a request type.
     */
    public <T> void addToRequestQueue(Request<T> request) {
        request.setTag(TAG);
        getRequestQueue().add(request);
    }

    /**
     * This method allows to cancel any pending request from request queue.
     *
     * @param tag is a tag which can recognize a request to cancel.
     */
    public void cancelPendingRequests(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    /**
     * This method checks if activity is visible (is in foreground). It is necessary if in app registered broadcast
     * receivers have to work only for this app, not for the whole system.
     *
     * @return visibility state of activity
     */
    public static boolean isActivityVisible() {
        return activityVisible;
    }

    /**
     * This method sets activity as visible when activity onResume method is called.
     */
    public static void activityResumed() {
        activityVisible = true;
    }

    /**
     * This method sets activity as invisible when activity onPause method is called.
     */
    public static void activityPaused() {
        activityVisible = false;
    }
}
