package pl.mradtke.secondpresentation;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Michal Radtke on 2015-05-06.
 * <p/>
 * This class is responsible for app session managing. It calls during log in and log out
 * processes.
 */
public class SessionManager {

    private static String TAG = SessionManager.class.getSimpleName();
    private static final String LOGIN_PREFERENCE = "login_preference";
    private static final String KEY_IS_LOGGED_IN = "is_logged_in";
    public static final String KEY_USER_ID = "user_id";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public SessionManager(Context context) {
        sharedPreferences = context.getSharedPreferences(LOGIN_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /**
     * This method is call when log in process is finished successfully.
     *
     * @param isLoggedIn is a flag which marks user as logged in.
     * @param userId     is a logged in user id, needs to making operations in app.
     */
    public void setLogin(boolean isLoggedIn, String userId) {
        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        editor.putString(KEY_USER_ID, userId);
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    /**
     * This method checks if user is still logged in or not.
     *
     * @return the result of checking if user is still logged in.
     */
    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    /**
     * This method is responsible for clear preferences related with user settings (log out
     * process).
     */
    //Todo logout button
    public void logOut() {
        editor.clear();
        editor.commit();
    }
}
