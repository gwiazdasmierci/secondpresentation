package pl.mradtke.secondpresentation.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by Michal Radtke on 2015-05-27.
 * <p/>
 * This service is responsible for getting current level of battery and passing it to the appropriate receiver.
 */
public class BatteryStatusService extends IntentService {

    public static final String BROADCAST_BATTERY_STATUS_ACTION = "battery_status_action";
    public static final String EXTRA_BATTERY_STATUS = "battery_status";

    /**
     * Creates an IntentService. Invoked by your subclass's constructor.
     */
    public BatteryStatusService() {
        super(BatteryStatusService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent data) {
        Intent intent = new Intent(BROADCAST_BATTERY_STATUS_ACTION);
        intent.putExtra(EXTRA_BATTERY_STATUS, getBatteryStatus());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * This method gets current level of battery.
     *
     * @return current level of battery in percents
     */
    private int getBatteryStatus() {
        IntentFilter batteryStatusIntentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(null, batteryStatusIntentFilter);

        int result = 0;
        if (batteryStatus != null) {
            result = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        }
        return result;
    }
}
