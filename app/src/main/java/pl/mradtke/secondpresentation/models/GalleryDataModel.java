package pl.mradtke.secondpresentation.models;

import android.graphics.Bitmap;

/**
 * Created by Michal Radtke on 2015-05-14.
 *
 * This is a data model for gallery view.
 */
public class GalleryDataModel {

    private String imageTitle;
    private String imageDesc;
    private String imageUrl;
    private Bitmap imageThumb;

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getImageDesc() {
        return imageDesc;
    }

    public void setImageDesc(String imageDesc) {
        this.imageDesc = imageDesc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Bitmap getImageThumb() {
        return imageThumb;
    }

    public void setImageThumb(Bitmap imageThumb) {
        this.imageThumb = imageThumb;
    }

    //    private ArrayList<String> imageTitle;
//    private ArrayList<String> imageDesc;
//    private ArrayList<String> image;
//    private ArrayList<Bitmap> imageThumb;
//
//    public ArrayList<String> getImageTitle() {
//        return imageTitle;
//    }
//
//    public void setImageTitle(ArrayList<String> imageTitle) {
//        this.imageTitle = imageTitle;
//    }
//
//    public ArrayList<String> getImageDesc() {
//        return imageDesc;
//    }
//
//    public void setImageDesc(ArrayList<String> imageDesc) {
//        this.imageDesc = imageDesc;
//    }
//
//    public ArrayList<String> getImage() {
//        return image;
//    }
//
//    public void setImage(ArrayList<String> image) {
//        this.image = image;
//    }
//
//    public ArrayList<Bitmap> getImageThumb() {
//        return imageThumb;
//    }
//
//    public void setImageThumb(ArrayList<Bitmap> imageThumb) {
//        this.imageThumb = imageThumb;
//    }
}
