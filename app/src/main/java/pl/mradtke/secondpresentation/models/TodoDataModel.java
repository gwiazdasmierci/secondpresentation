package pl.mradtke.secondpresentation.models;

/**
 * Created by michal.radtke@gmail.com on 2015-05-22.
 * <p/>
 * This is to do data model.
 */
public class TodoDataModel {

    private int todoID;
    private String todoTitle;
    private String todoDescription;
    private int todoCategory;

    public int getTodoID() {
        return todoID;
    }

    public void setTodoID(int todoID) {
        this.todoID = todoID;
    }

    public String getTodoTitle() {
        return todoTitle;
    }

    public void setTodoTitle(String todoTitle) {
        this.todoTitle = todoTitle;
    }

    public String getTodoDescription() {
        return todoDescription;
    }

    public void setTodoDescription(String todoDescription) {
        this.todoDescription = todoDescription;
    }

    public int getTodoCategory() {
        return todoCategory;
    }

    public void setTodoCategory(int todoCategory) {
        this.todoCategory = todoCategory;
    }
}
