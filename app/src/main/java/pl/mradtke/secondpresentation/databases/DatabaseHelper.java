package pl.mradtke.secondpresentation.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Michal Radtke on 2015-05-13.
 * <p/>
 * Database helper.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "second_presentation.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ", ";
    private static final String NOT_NULL = " NOT NULL";
    private static final String SQL_CREATE_TODO_TABLE = "CREATE TABLE "
            + TodoTable.TABLE_TODO_TABLE_NAME
            + "(" + TodoTable.TABLE_TODO_COLUMN_ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT, "
            + TodoTable.TABLE_TODO_COLUMN_CATEGORY + TEXT_TYPE + NOT_NULL + COMMA_SEP
            + TodoTable.TABLE_TODO_COLUMN_TITLE + TEXT_TYPE + NOT_NULL + COMMA_SEP
            + TodoTable.TABLE_TODO_COLUMN_DESCRIPTION + INTEGER_TYPE + NOT_NULL
            + ");";
    private static final String SQL_DROP_TODO_TABLE =
            "DROP TABLE IF EXISTS " + TodoTable.TABLE_TODO_TABLE_NAME;
    public static final String SQL_EQUALS = "=?";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(SQL_CREATE_TODO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL(SQL_DROP_TODO_TABLE);
        onCreate(database);
    }

    public void onDowngrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        onUpgrade(database, oldVersion, newVersion);
    }
}
