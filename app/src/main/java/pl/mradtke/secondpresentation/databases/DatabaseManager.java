package pl.mradtke.secondpresentation.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import pl.mradtke.secondpresentation.models.TodoDataModel;

/**
 * Created by michal.radtke@gmail.com on 2015-05-22.
 * <p/>
 * This is a database manager. It contains all methods to exchange data between database and UI.
 */
public class DatabaseManager {

    private DatabaseHelper databaseHelper;

    public DatabaseManager(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    /**
     * This method gets to do titles from database and filling them in list view.
     *
     * @return to do list
     */
    public ArrayList<String> getTodoTitlesFromDb() {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] columnNames = {TodoTable.TABLE_TODO_COLUMN_TITLE};
        ArrayList<String> todoList = new ArrayList<>();

        Cursor cursor = db.query(TodoTable.TABLE_TODO_TABLE_NAME, columnNames, null, null, null, null, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            todoList.add(i, cursor.getString(cursor.getColumnIndexOrThrow(TodoTable.TABLE_TODO_COLUMN_TITLE)));
            cursor.moveToNext();
        }
        cursor.close();

        return todoList;
    }

    /**
     * This method deletes to do note from database.
     *
     * @param todoTitle is a title to delete.
     */
    public void deleteTodoFromDb(String todoTitle) {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String selection = TodoTable.TABLE_TODO_COLUMN_TITLE + DatabaseHelper.SQL_EQUALS;
        String[] selectionArgs = {todoTitle};
        db.delete(TodoTable.TABLE_TODO_TABLE_NAME, selection, selectionArgs);
    }

    /**
     * This method is responsible for getting all data about specified to do note from database and filling them in
     * appropriate fields.
     *
     * @param todoTitle is a keyword which is searched in database.
     * @return to do data
     */
    public TodoDataModel getTodoDataFromDb(String todoTitle) {
        TodoDataModel todoData = new TodoDataModel();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] columnNames = {TodoTable.TABLE_TODO_COLUMN_ID, TodoTable.TABLE_TODO_COLUMN_TITLE,
                TodoTable.TABLE_TODO_COLUMN_DESCRIPTION, TodoTable.TABLE_TODO_COLUMN_CATEGORY};
        String[] where = {todoTitle};

        Cursor cursor = db.query(TodoTable.TABLE_TODO_TABLE_NAME, columnNames,
                TodoTable.TABLE_TODO_COLUMN_TITLE + DatabaseHelper.SQL_EQUALS, where, null, null, null);
        cursor.moveToFirst();

        todoData.setTodoID(cursor.getInt(cursor.getColumnIndexOrThrow(TodoTable.TABLE_TODO_COLUMN_ID)));
        todoData.setTodoTitle(cursor.getString(cursor.getColumnIndexOrThrow(TodoTable.TABLE_TODO_COLUMN_TITLE)));
        todoData.setTodoDescription(cursor.getString(cursor.getColumnIndexOrThrow(TodoTable.TABLE_TODO_COLUMN_DESCRIPTION)));
        todoData.setTodoCategory(cursor.getInt(cursor.getColumnIndexOrThrow(TodoTable.TABLE_TODO_COLUMN_CATEGORY)));

        cursor.close();
        return todoData;
    }

    /**
     * This method inserts data (to do note) to database.
     *
     * @param title      is a title of note.
     * @param desc       is a description of note.
     * @param categoryId is a category id of note (urgent, reminder).
     */
    public void insertTodoDataToDb(String title, String desc, int categoryId) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TodoTable.TABLE_TODO_COLUMN_TITLE, title);
        values.put(TodoTable.TABLE_TODO_COLUMN_DESCRIPTION, desc);
        values.put(TodoTable.TABLE_TODO_COLUMN_CATEGORY, categoryId);
        db.insert(TodoTable.TABLE_TODO_TABLE_NAME, null, values);
    }

    /**
     * This method updates data (to do note) in database.
     *
     * @param todoId     is a to do note id.
     * @param title      is a to do note title.
     * @param desc       is a description of to do note.
     * @param categoryId is a category id of to do note.
     */
    public void updateTodoDataInDb(int todoId, String title, String desc, int categoryId) {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        ContentValues values = new ContentValues();
        values.put(TodoTable.TABLE_TODO_COLUMN_TITLE, title);
        values.put(TodoTable.TABLE_TODO_COLUMN_DESCRIPTION, desc);
        values.put(TodoTable.TABLE_TODO_COLUMN_CATEGORY, categoryId);

        String selection = TodoTable.TABLE_TODO_COLUMN_ID + DatabaseHelper.SQL_EQUALS;
        String[] selectionArgs = {String.valueOf(todoId)};

        db.update(TodoTable.TABLE_TODO_TABLE_NAME, values, selection, selectionArgs);
    }

    /**
     * This method checks if given to do title does not exists in database.
     *
     * @param todoTitle is a given to do title.
     * @return result of checking.
     */
    public boolean doesTodoTitleExists(String todoTitle) {
        boolean result = false;
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String[] columnNames = {TodoTable.TABLE_TODO_COLUMN_TITLE};
        String[] where = {todoTitle};

        Cursor cursor = db.query(TodoTable.TABLE_TODO_TABLE_NAME, columnNames,
                TodoTable.TABLE_TODO_COLUMN_TITLE + DatabaseHelper.SQL_EQUALS, where, null, null, null);
        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            result = true;
        }
        cursor.close();

        return result;
    }
}
