package pl.mradtke.secondpresentation.databases;

/**
 * Created by Michal Radtke on 2015-05-13.
 * <p/>
 * This is a database structure table for to do.
 */
public final class TodoTable {

    public static final String TABLE_TODO_TABLE_NAME = "todo";
    public static final String TABLE_TODO_COLUMN_ID = "id";
    public static final String TABLE_TODO_COLUMN_CATEGORY = "category";
    public static final String TABLE_TODO_COLUMN_TITLE = "title";
    public static final String TABLE_TODO_COLUMN_DESCRIPTION = "description";
}
