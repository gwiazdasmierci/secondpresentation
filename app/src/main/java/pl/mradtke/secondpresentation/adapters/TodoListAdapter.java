package pl.mradtke.secondpresentation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pl.mradtke.secondpresentation.R;

/**
 * Created by Michal Radtke on 2015-05-13.
 *
 * This class is a custom adapter for todolist.
 */
public class TodoListAdapter extends BaseAdapter {

    private ArrayList<String> todoList;
    private LayoutInflater inflater;

    public TodoListAdapter(Context context, ArrayList<String> todoList) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.todoList = todoList;
    }

    @Override
    public int getCount() {
        return todoList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_todo_list, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.ivTodoThumb = (ImageView) convertView.findViewById(R.id.adapter_todo_list_iv_todo_thumb);
            viewHolder.tvTodoName = (TextView) convertView.findViewById(R.id.adapter_todo_list_tv_todo_name);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.ivTodoThumb.setImageResource(R.drawable.todo_thumb);
        viewHolder.tvTodoName.setText(todoList.get(position));

        return convertView;
    }

    /**
     * This class is a view holder for all widgets in this adapter.
     */
    private class ViewHolder {
        private ImageView ivTodoThumb;
        private TextView tvTodoName;
    }
}
