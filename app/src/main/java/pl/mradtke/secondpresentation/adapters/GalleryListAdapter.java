package pl.mradtke.secondpresentation.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.models.GalleryDataModel;

/**
 * Created by Michal Radtke on 2015-05-14.
 * <p/>
 * This class is a custom adapter for gallery list.
 */
public class GalleryListAdapter extends BaseAdapter {

    private ArrayList<GalleryDataModel> galleryListData;
    private LayoutInflater inflater;

    public GalleryListAdapter(Context context, ArrayList<GalleryDataModel> galleryListData) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.galleryListData = galleryListData;
    }

    @Override
    public int getCount() {
        return galleryListData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_gallery_list, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.ivImageThumb = (ImageView) convertView.findViewById(R.id.adapter_gallery_list_iv_image_thumb);
            viewHolder.tvImageTitle = (TextView) convertView.findViewById(R.id.adapter_gallery_list_tv_image_title);
            viewHolder.tvImageTitle.setTextColor(Color.BLACK);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.ivImageThumb.setImageBitmap(galleryListData.get(position).getImageThumb());
        viewHolder.tvImageTitle.setText(galleryListData.get(position).getImageTitle());

        return convertView;
    }

    /**
     * This class is a view holder for all widgets in this adapter.
     */
    private class ViewHolder {
        private ImageView ivImageThumb;
        private TextView tvImageTitle;
    }
}
