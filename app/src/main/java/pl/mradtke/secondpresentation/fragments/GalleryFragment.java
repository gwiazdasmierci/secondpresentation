package pl.mradtke.secondpresentation.fragments;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.adapters.GalleryListAdapter;
import pl.mradtke.secondpresentation.loaders.GalleryLoaderSync;
import pl.mradtke.secondpresentation.models.GalleryDataModel;
import pl.mradtke.secondpresentation.utils.NetworkUtil;
import pl.mradtke.secondpresentation.views.MainActivity;

import static android.widget.AdapterView.OnItemClickListener;

/**
 * Created by Michal Radtke on 2015-05-14.
 * <p/>
 * Gallery fragment is responsible for displaying list of thumbnails and their titles.
 */
public class GalleryFragment extends BaseFragment
        implements LoaderManager.LoaderCallbacks<ArrayList<GalleryDataModel>>, OnItemClickListener {

    private static final int URL_LOADER_ID = 0;
    public static final String EXTRA_PHOTO_URL = "photo_url";

    private ListView lvGallery;
    private MainActivity mainActivity;
    private ArrayList<GalleryDataModel> galleryList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        uiInit(view);
        getLoaderManager().initLoader(URL_LOADER_ID, null, this);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }

    @Override
    public Loader<ArrayList<GalleryDataModel>> onCreateLoader(int loaderId, Bundle args) {
        switch (loaderId) {
            case URL_LOADER_ID:
                GalleryLoaderSync galleryLoaderSync = new GalleryLoaderSync(getActivity());
                galleryLoaderSync.forceLoad();
                return galleryLoaderSync;
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<GalleryDataModel>> loader, ArrayList<GalleryDataModel> galleryListData) {
        lvGallery.setAdapter(new GalleryListAdapter(getActivity(), galleryListData));
        galleryList = galleryListData;
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<GalleryDataModel>> loader) {
        lvGallery.setAdapter(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mainActivity.showPhotoFragment(NetworkUtil.GLOBAL_URL + galleryList.get(position).getImageUrl());
    }

    /**
     * This method init the UI.
     *
     * @param view is a view where are widgets initialized.
     */
    private void uiInit(View view) {
        lvGallery = (ListView) view.findViewById(R.id.fragment_gallery_lv_gallery);
        lvGallery.setOnItemClickListener(this);
    }
}
