package pl.mradtke.secondpresentation.fragments;

import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.loaders.ImageLoaderSync;

/**
 * Created by Michal Radtke on 2015-05-22.
 * <p/>
 * This fragment shows full size image of thumbnail from gallery list.
 */
public class PhotoFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Bitmap> {

    private static final int URL_LOADER_ID = 1;

    private ImageView ivPhoto;

    public static PhotoFragment newInstance(String photoUrl) {
        PhotoFragment photoFragment = new PhotoFragment();
        Bundle args = new Bundle();
        args.putString(GalleryFragment.EXTRA_PHOTO_URL, photoUrl);
        photoFragment.setArguments(args);
        return photoFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        uiInit(view);
        getLoaderManager().initLoader(URL_LOADER_ID, null, this);
        return view;
    }

    @Override
    public Loader<Bitmap> onCreateLoader(int loaderId, Bundle args) {
        switch (loaderId) {
            case URL_LOADER_ID:
                ImageLoaderSync imageLoaderSync = new ImageLoaderSync(getActivity(),
                        getArguments().getString(GalleryFragment.EXTRA_PHOTO_URL));
                imageLoaderSync.forceLoad();
                return imageLoaderSync;
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Bitmap> loader, Bitmap data) {
        ivPhoto.setImageBitmap(data);
    }

    @Override
    public void onLoaderReset(Loader<Bitmap> loader) {

    }

    /**
     * This method init the UI.
     *
     * @param view is a view where are widgets initialized.
     */
    private void uiInit(View view) {
        ivPhoto = (ImageView) view.findViewById(R.id.fragment_photo_iv_photo);

    }
}
