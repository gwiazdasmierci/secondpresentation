package pl.mradtke.secondpresentation.fragments;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.databases.DatabaseManager;
import pl.mradtke.secondpresentation.models.TodoDataModel;
import pl.mradtke.secondpresentation.receivers.ReminderReceiver;
import pl.mradtke.secondpresentation.views.MainActivity;

/**
 * Created by Michal Radtke on 2015-05-13.
 * <p/>
 * This fragment displays to do details.
 */
public class TodoElementFragment extends BaseFragment implements View.OnClickListener {

    private static final String EXTRA_TODO_TITLE = "todo_title";
    public static final String EXTRA_REMINDER_TITLE = "reminder_title";
    public static final String EXTRA_REMINDER_TEXT = "reminder_text";
    private static final int REQUEST_CODE = 201;
    private static final int REMINDER_ID = 1;

    private DatabaseManager databaseManager;
    private MainActivity mainActivity;
    private EditText etTodoTitle;
    private EditText etTodoDesc;
    private Spinner spTodoCategory;
    private int todoId = 0;
    private TodoDataModel todoDataModel;
    private TextView tvReminderTime;
    private long reminderTimeInMillis;

    public static TodoElementFragment newInstance(String todoTitle) {
        TodoElementFragment todoElementFragment = new TodoElementFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_TODO_TITLE, todoTitle);
        todoElementFragment.setArguments(args);
        return todoElementFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_todo_element, container, false);
        uiInit(view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        databaseManager = new DatabaseManager(getActivity().getApplicationContext());
        mainActivity = (MainActivity)getActivity();
    }

    /**
     * Creates new instance of to do category spinner and checks if it is an edit mode (bundled to do title is not
     * null).
     */
    @Override
    public void onResume() {
        super.onResume();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.fragment_todo_element_todo_categories));
        spTodoCategory.setAdapter(adapter);

        String todoTitle = getArguments().getString(EXTRA_TODO_TITLE, null);
        if (!TextUtils.isEmpty(todoTitle)) {
            todoDataModel = databaseManager.getTodoDataFromDb(todoTitle);
            fillViewsWithData();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_todo_element_btn_save:
                int categoryId = (int) spTodoCategory.getSelectedItemId();
                String title = etTodoTitle.getText().toString();
                String desc = etTodoDesc.getText().toString();

                //before saving process checks if given to do tile does not already exist in db
                if (databaseManager.doesTodoTitleExists(title) && todoId == 0) {
                    showToast(getString(R.string.fragment_todo_element_todo_exists));
                    break;
                }

                if (!TextUtils.isEmpty(title) || !TextUtils.isEmpty(desc)) {
                    //if todoId is greater than 0 and user changed a title, then do to do update. If not,
                    // insert new to do
                    if (todoId > 0 && title.equals(getArguments().getString(EXTRA_TODO_TITLE, null))) {
                        databaseManager.updateTodoDataInDb(todoId, title, desc, categoryId);
                        showToast(getString(R.string.fragment_todo_element_update_confirmation));
                    } else {
                        databaseManager.insertTodoDataToDb(title, desc, categoryId);
                        showToast(getString(R.string.fragment_todo_element_insert_confirmation));
                    }
                    //if reminder was set, prepares alarm reminder
                    if (categoryId == REMINDER_ID) {
                        prepareAlarmReminder(title, desc, reminderTimeInMillis);
                    }
                    getFragmentManager().popBackStack();
                } else {
                    showToast(getString(R.string.fragment_todo_element_empty_fields));
                }
                break;
        }
    }

    /**
     * This method init the UI.
     *
     * @param view is a view where are widgets initialized.
     */
    private void uiInit(View view) {
        etTodoTitle = (EditText) view.findViewById(R.id.fragment_todo_element_et_todo_title);
        etTodoDesc = (EditText) view.findViewById(R.id.fragment_todo_element_et_todo_desc);
        Button btnTodoSave = (Button) view.findViewById(R.id.fragment_todo_element_btn_save);
        btnTodoSave.setOnClickListener(this);
        spTodoCategory = (Spinner) view.findViewById(R.id.fragment_todo_element_sp_todo_category);
        spTodoCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == REMINDER_ID) {
                    mainActivity.showTimePickerDialog();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tvReminderTime = (TextView) view.findViewById(R.id.fragment_todo_element_tv_reminder_time);
    }

    /**
     * This method fills all views in this fragment with to do data from database.
     */
    private void fillViewsWithData() {
        todoId = todoDataModel.getTodoID();
        etTodoTitle.setText(todoDataModel.getTodoTitle());
        etTodoDesc.setText(todoDataModel.getTodoDescription());
        spTodoCategory.setSelection(todoDataModel.getTodoCategory());
    }

    /**
     * This method prepares alarm reminder. It is called when user selected reminder category in to do options.
     *
     * @param todoTitle            is a to do title to display in notification which is called in receiver related with this alarm
     * @param todoText             ia s to do text to display in notification which is called in receiver related with this alarm
     * @param reminderTimeInMillis is a time expressed in milliseconds and is get from time picker in to do options
     */
    private void prepareAlarmReminder(String todoTitle, String todoText, long reminderTimeInMillis) {
        Intent intent = new Intent(getActivity(), ReminderReceiver.class);
        intent.putExtra(EXTRA_REMINDER_TITLE, todoTitle);
        intent.putExtra(EXTRA_REMINDER_TEXT, todoText);

        PendingIntent sender = PendingIntent.getBroadcast(getActivity(), REQUEST_CODE, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, reminderTimeInMillis, sender);
    }

    /**
     * This method updates time textview with time gets from time picker in to do options and sets this time as
     * global variable in milliseconds. This time in milliseconds.
     *
     * @param hour                 is a selected hour in time picker in to do options
     * @param minute               is a selected minute in time picker in to do options
     * @param selectedTimeInMillis is a selected time in time picker in to do options expressed in milliseconds
     */
    public void updateReminderTimeInView(int hour, int minute, long selectedTimeInMillis) {
        tvReminderTime.setText(hour + ":" + minute);
        reminderTimeInMillis = selectedTimeInMillis;
    }
}
