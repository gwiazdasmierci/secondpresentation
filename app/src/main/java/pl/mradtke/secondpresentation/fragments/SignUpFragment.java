package pl.mradtke.secondpresentation.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.utils.NetworkUtil;
import pl.mradtke.secondpresentation.webservices.RegisterJsonRequest;

/**
 * Created by Michal Radtke on 2015-05-06.
 * <p/>
 * This fragment is responsible for display registration form.
 */
public class SignUpFragment extends BaseFragment implements View.OnClickListener {

    private EditText etLogin;
    private EditText etPassword;
    private EditText etEmail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        uiInit(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_sign_up_btn_register:
                String login = etLogin.getText().toString().trim();
                String password = etPassword.getText().toString();
                String email = etEmail.getText().toString().trim();

                if (!NetworkUtil.isInternetConnection(getActivity())) {
                    showToast(getString(R.string.utils_no_internet_connection));
                    break;
                }

                if (TextUtils.isEmpty(login) || TextUtils.isEmpty(password) ||
                        TextUtils.isEmpty(email)) {
                    showToast(getString(R.string.fragment_sign_up_wrong_input_data));
                } else {
                    new RegisterJsonRequest(getActivity()).execute(login, password, email);
                }
                break;
            case R.id.fragment_sign_up_tv_login:
                getActivity().onBackPressed();
                break;
        }
    }

    /**
     * This method init the UI.
     *
     * @param view is a view where are widgets initialized.
     */
    private void uiInit(View view) {
        etLogin = (EditText) view.findViewById(R.id.fragment_sign_up_et_login);
        etPassword = (EditText) view.findViewById(R.id.fragment_sign_up_et_password);
        etEmail = (EditText) view.findViewById(R.id.fragment_sign_up_et_email);
        Button btnRegister = (Button) view.findViewById(R.id.fragment_sign_up_btn_register);
        btnRegister.setOnClickListener(this);
        TextView tvLogin = (TextView) view.findViewById(R.id.fragment_sign_up_tv_login);
        tvLogin.setOnClickListener(this);
    }
}
