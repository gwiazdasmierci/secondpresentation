package pl.mradtke.secondpresentation.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.utils.FileUtil;

/**
 * Created by Michal Radtke on 2015-05-08.
 * <p/>
 * This class presents Notepad feature.
 */
public class NotepadFragment extends BaseFragment implements View.OnClickListener {

    private static final String EXTRA_FILENAME = "filename";
    private static final int NUMBER_OF_CHARS_AT_FILE_EXTENSION = 4;

    private EditText etFilename;
    private EditText etFileContent;
    private String extraFilenameValue;
    private Button btnSave;

    /**
     * Create new instance of NotepadFragment class with filename which is given in FileExplorerFragment.
     *
     * @param filename is a filename to read - if null it is mean that notepad is in save mode. In other case in
     *                 update mode, and content of file will be reading
     * @return new instance of NotepadFragment class
     */
    public static NotepadFragment newInstance(String filename) {
        NotepadFragment notepadFragment = new NotepadFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_FILENAME, filename);
        notepadFragment.setArguments(args);
        return notepadFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_notepad, container, false);
        uiInit(view);


        return view;
    }

    /**
     * Gets filename value from instance bundle and check if is null or not. If is not null,
     * read file content and update UI.
     */
    @Override
    public void onResume() {
        super.onResume();
        extraFilenameValue = getArguments().getString(EXTRA_FILENAME);
        if (!TextUtils.isEmpty(extraFilenameValue)) {
            onResumeUiUpdate(extraFilenameValue);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_notepad_btn_clear_form:
                clearFields();
                break;
            case R.id.fragment_notepad_btn_save:
                saveOrUpdateAsFile();
                getFragmentManager().popBackStack();
                break;
        }
    }

    /**
     * This method is responsible for updating UI on each onResume when given filename is not null. We have to cut
     * the last 4 chars from filename string (file extension), because it may produces incorrect filenames.
     */
    private void onResumeUiUpdate(String filename) {
        etFilename.setText(filename.substring(0, filename.length() - NUMBER_OF_CHARS_AT_FILE_EXTENSION));
        etFileContent.setText(FileUtil.loadFromFile(filename));
        btnSave.setText(getString(R.string.fragment_notepad_update));
        showToast(getString(R.string.file_util_file_loaded_successfully));
    }

    /**
     * This method clears all text fields.
     */
    public void clearFields() {
        etFilename.setText("");
        etFileContent.setText("");
    }

    /**
     * This method init the UI.
     *
     * @param view is a view where are widgets initialized.
     */
    private void uiInit(View view) {
        etFilename = (EditText) view.findViewById(R.id.fragment_notepad_et_filename);
        etFileContent = (EditText) view.findViewById(R.id.fragment_notepad_et_file_content);
        Button btnClearForm = (Button) view.findViewById(R.id.fragment_notepad_btn_clear_form);
        btnClearForm.setOnClickListener(this);
        btnSave = (Button) view.findViewById(R.id.fragment_notepad_btn_save);
        btnSave.setOnClickListener(this);
    }

    /**
     * This method saves new file or updates already exist file. The choice depends on filename value. In case when
     * filename is null method saves new file, otherwise method updates already exist file. At the end clears text
     * input fields.
     */
    private void saveOrUpdateAsFile() {
        String filename = etFilename.getText().toString();
        String fileContent = etFileContent.getText().toString();

        if (TextUtils.isEmpty(filename) || TextUtils.isEmpty(fileContent)) {
            showToast(getString(R.string.fragment_notepad_wrong_length_of_input_data));
            return;
        }

        if (!TextUtils.isEmpty(extraFilenameValue) && extraFilenameValue.equals(filename)) {
            FileUtil.updateFile(getActivity(), filename, fileContent);
        } else {
            FileUtil.saveAsFile(getActivity(), filename, fileContent);
        }
        clearFields();
    }
}