package pl.mradtke.secondpresentation.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.utils.FileUtil;
import pl.mradtke.secondpresentation.views.MainActivity;

/**
 * Created by Michal Radtke on 2015-05-18.
 * <p/>
 * This is a simple file explorer.
 */
public class FileExplorerFragment extends BaseFragment {

    private static final String CLOUD_APPLICATION_NAME_KEY = "app_name";
    private static final String CLOUD_FILE_NAME_KEY = "file_name";
    private static final String CLOUD_TABLE_NAME_KEY = "Notes";
    private static final String CLOUD_FILE_KEY = "file";
    private static final int DELAY_TO_UPDATE_FILES_LIST = 1500;

    private ListView lvFiles;
    private MainActivity mainActivity;
    private int itemLongClickPosition;
    private ArrayList<String> filesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_file_explorer, container, false);
        setHasOptionsMenu(true);
        uiInit(view, inflater);
        FileUtil.createExternalPathIfNotExists();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }

    /**
     * On each onResume we have to update files list.
     */
    @Override
    public void onResume() {
        super.onResume();
        Parse.initialize(getActivity(), getString(R.string.parse_cloud_application_id), getString(R.string.parse_cloud_client_id));
        updateFilesList();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.file_explorer_action_bar_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_new_note:
                mainActivity.showNotepadFragment(null);
                return true;
            case R.id.action_sync_all_notepads:
                syncAllFilesWithCloud();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This method is responsible for UI init with their events. In onItemLongClick event we have to subtract "1" digit
     * from position number, because a list header makes an artificial item.
     *
     * @param view     is a view on which we initialized UI controls and events
     * @param inflater is an initializer which is linking XML files into given view
     */
    private void uiInit(View view, LayoutInflater inflater) {
        View listHeader = inflater.inflate(R.layout.fragment_file_explorer_list_header, null);
        lvFiles = (ListView) view.findViewById(R.id.fragment_file_explorer_lv_files);
        lvFiles.addHeaderView(listHeader, null, false);
        lvFiles.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                itemLongClickPosition = position - 1;
                getActivity().startActionMode(modeCallBack);
                return false;
            }
        });
        registerForContextMenu(lvFiles);
    }

    /**
     * Custom callback for contextual action mode with two options: delete file and sync file with Google Drive account.
     */
    private ActionMode.Callback modeCallBack = new ActionMode.Callback() {

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public void onDestroyActionMode(ActionMode mode) {
            mode = null;
        }

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.setTitle(getString(R.string.fragment_file_explorer_context_menu_title));
            mode.getMenuInflater().inflate(R.menu.file_explorer_context_menu, menu);
            return true;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int id = item.getItemId();
            switch (id) {
                case R.id.action_edit_file:
                    mainActivity.showNotepadFragment(filesList.get(itemLongClickPosition));
                    mode.finish();
                    break;
                case R.id.action_delete_file:
                    FileUtil.deleteFile(filesList.get(itemLongClickPosition));
                    showToast(getString(R.string.file_util_file_deleted_successfully));
                    updateFilesList();
                    mode.finish();
                    break;
                case R.id.action_sync_file:
                    saveFileInCloud();
                    showToast(getString(R.string.fragment_file_explorer_saving_file_in_cloud_confirmation));
                    mode.finish();
                    break;
                default:
                    return false;
            }
            return true;
        }
    };

    /**
     * This method updates files list.
     */
    private void updateFilesList() {
        File dir = new File(FileUtil.FILE_PATH);
        filesList = FileUtil.fileNames(dir.listFiles());

        ArrayAdapter<String> filesAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, filesList);
        lvFiles.setAdapter(filesAdapter);
    }

    /**
     * This method is responsible for saving selected file in cloud.
     */
    private void saveFileInCloud() {
        String filename = filesList.get(itemLongClickPosition);
        String fileContent = FileUtil.loadFromFile(filename);
        byte[] data = fileContent.getBytes();
        ParseFile parseFile = new ParseFile(filename, data);
        parseFile.saveInBackground();
        associateFileWithTable(parseFile, filename);
    }

    /**
     * This method is used by saveFileInCloud class and associates file with appropriate table in database. It is
     * much easier to working with files.
     *
     * @param parseFile is a file to associate
     */
    private void associateFileWithTable(ParseFile parseFile, String filename) {
        ParseObject parseObject = new ParseObject(CLOUD_TABLE_NAME_KEY);
        parseObject.put(CLOUD_APPLICATION_NAME_KEY, getActivity().getPackageName());
        parseObject.put(CLOUD_FILE_NAME_KEY, filename);
        parseObject.put(CLOUD_FILE_KEY, parseFile);
        parseObject.saveInBackground();
    }

    /**
     * This method is responsible for syncing files with cloud - if in cloud are files which don't exist on device,
     * then save them.
     */
    private void syncAllFilesWithCloud() {
        ParseQuery<ParseObject> query = new ParseQuery<>(CLOUD_TABLE_NAME_KEY);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                for (ParseObject ob : parseObjects) {
                    ParseFile parseFile = (ParseFile) ob.get(CLOUD_FILE_KEY);
                    FileUtil.saveAsFileFromUrl(ob.get(CLOUD_FILE_NAME_KEY).toString(), parseFile.getUrl());
                }
            }
        });
        showToast(getString(R.string.fragment_file_explorer_sync_files_confirmation));
        //we have to delayed update files list because update in normal time is faster than creates new files
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateFilesList();
            }
        }, DELAY_TO_UPDATE_FILES_LIST);
    }
}
