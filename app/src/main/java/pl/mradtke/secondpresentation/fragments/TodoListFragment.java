package pl.mradtke.secondpresentation.fragments;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.adapters.TodoListAdapter;
import pl.mradtke.secondpresentation.databases.DatabaseManager;
import pl.mradtke.secondpresentation.views.MainActivity;

import static android.view.ContextMenu.ContextMenuInfo;

/**
 * Created by Michal Radtke on 2015-05-13.
 * <p/>
 * This fragment displays list of todos from database.
 */
public class TodoListFragment extends BaseFragment implements AdapterView.OnItemClickListener, View.OnClickListener {

    private DatabaseManager databaseManager;
    private MainActivity mainActivity;
    private ListView lvTodo;
    private ArrayList<String> todoList;
    private TodoListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);
        uiInit(view);
        registerForContextMenu(view.findViewById(R.id.fragment_todo_list_lv_todo));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        databaseManager = new DatabaseManager(getActivity().getApplicationContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        todoList = databaseManager.getTodoTitlesFromDb();
        initTodoAdapterAndList();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.todo_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.todo_context_menu_delete_todo:
                databaseManager.deleteTodoFromDb(todoList.get(info.position));
                todoList.remove(info.position);
                adapter.notifyDataSetChanged();
                showToast(getString(R.string.fragment_todo_list_delete_confirmation));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mainActivity.showTodoElementFragment(todoList.get(position));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_todo_list_btn_insert_todo:
                mainActivity.showTodoElementFragment(null);
                break;
        }
    }

    /**
     * This method init the UI.
     *
     * @param view is a view where are widgets initialized.
     */
    private void uiInit(View view) {
        lvTodo = (ListView) view.findViewById(R.id.fragment_todo_list_lv_todo);
        lvTodo.setOnItemClickListener(this);
        Button btnTodoSave = (Button) view.findViewById(R.id.fragment_todo_list_btn_insert_todo);
        btnTodoSave.setOnClickListener(this);
    }

    /**
     * This method init to do adapter and list.
     */
    private void initTodoAdapterAndList() {
        adapter = new TodoListAdapter(getActivity(), todoList);
        lvTodo.setAdapter(adapter);
    }
}
