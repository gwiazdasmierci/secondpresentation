package pl.mradtke.secondpresentation.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by Michal Radtke on 2015-05-22.
 * <p/>
 * This is a simple Time picker fragment. His result is passed to TodoElementFragment where is used in update UI and
 * prepare alarm reminder.
 */
public class TimePickerDialogFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private Calendar calendar;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        //gets selected hour and minute, setup them as new time and get this time in milliseconds
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        long selectedTimeInMillis = calendar.getTimeInMillis();

        TodoElementFragment todoElementFragment = (TodoElementFragment) getFragmentManager().findFragmentByTag
                (TodoElementFragment.class.getSimpleName());
        todoElementFragment.updateReminderTimeInView(hourOfDay, minute, selectedTimeInMillis);
    }
}
