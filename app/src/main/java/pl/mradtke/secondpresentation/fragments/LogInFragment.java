package pl.mradtke.secondpresentation.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.utils.NetworkUtil;
import pl.mradtke.secondpresentation.utils.Utils;
import pl.mradtke.secondpresentation.webservices.LogInJsonRequest;

/**
 * Created by Michal Radtke on 2015-05-06.
 * <p/>
 * This fragment is responsible for display login form.
 */
public class LogInFragment extends BaseFragment implements View.OnClickListener {

    private EditText etLogin;
    private EditText etPassword;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_log_in, container, false);
        uiInit(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_log_in_btn_login:
                String login = etLogin.getText().toString().trim();
                String password = etPassword.getText().toString();
                if (!NetworkUtil.isInternetConnection(getActivity())) {
                    showToast(getString(R.string.utils_no_internet_connection));
                    break;
                }

                if (TextUtils.isEmpty(login) || TextUtils.isEmpty(password)) {
                    showToast(getString(R.string.fragment_log_in_wrong_credentials));
                } else {
                    new LogInJsonRequest(getActivity()).execute(login, password);
                }
                break;
            case R.id.fragment_log_in_tv_sign_up:
                Fragment signUpFragment = new SignUpFragment();
                Utils.replaceFragment(getFragmentManager().beginTransaction(), signUpFragment,
                        SignUpFragment.class.getSimpleName());
                break;
            default:
                break;
        }
    }

    /**
     * This method init the UI.
     *
     * @param view is a view where are widgets initialized.
     */
    private void uiInit(View view) {
        etLogin = (EditText) view.findViewById(R.id.fragment_log_in_et_login);
        etPassword = (EditText) view.findViewById(R.id.fragment_log_in_et_password);
        Button btnLogin = (Button) view.findViewById(R.id.fragment_log_in_btn_login);
        btnLogin.setOnClickListener(this);
        TextView tvSignUp = (TextView) view.findViewById(R.id.fragment_log_in_tv_sign_up);
        tvSignUp.setOnClickListener(this);
    }
}
