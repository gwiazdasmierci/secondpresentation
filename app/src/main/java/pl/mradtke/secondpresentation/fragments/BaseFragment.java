package pl.mradtke.secondpresentation.fragments;

import android.app.Fragment;
import android.widget.Toast;

import pl.mradtke.secondpresentation.AppController;

/**
 * Created by michal.radtke@gmail.com on 2015-05-22.
 *
 * This is a base fragment for all fragments in this application.
 */
public class BaseFragment extends Fragment{

    /**
     * This method displays toast on a screen.
     *
     * @param text is a text which will be display.
     */
    protected void showToast(String text) {
        Toast.makeText(AppController.getInstance().getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }
}
