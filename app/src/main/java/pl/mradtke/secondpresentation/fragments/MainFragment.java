package pl.mradtke.secondpresentation.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import pl.mradtke.secondpresentation.R;
import pl.mradtke.secondpresentation.views.MainActivity;

/**
 * Created by Michal Radtke on 2015-05-08.
 * <p/>
 * This is the main fragment which is visible after login process.
 */
public class MainFragment extends BaseFragment implements View.OnClickListener {

    private MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        uiInit(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity)getActivity();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_main_btn_notepad:
                mainActivity.showFileExplorerFragment();
                break;
            case R.id.fragment_main_btn_todo_list:
                mainActivity.showTodoListFragment();
                break;
            case R.id.fragment_main_btn_gallery:
                mainActivity.showGalleryFragment();
                break;
        }
    }

    /**
     * This method init the UI.
     *
     * @param view is a view where are widgets initialized.
     */
    private void uiInit(View view) {
        Button btnNotepad = (Button) view.findViewById(R.id.fragment_main_btn_notepad);
        btnNotepad.setOnClickListener(this);
        Button btnTodoList = (Button) view.findViewById(R.id.fragment_main_btn_todo_list);
        btnTodoList.setOnClickListener(this);
        Button btnGallery = (Button) view.findViewById(R.id.fragment_main_btn_gallery);
        btnGallery.setOnClickListener(this);
    }
}
